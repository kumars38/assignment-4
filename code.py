from lab4 import mergesort, merge, create_near_sorted_list, create_random_list
from sorts import mergesort_bottom, mergesort_three
from xlsxwriter import Workbook
import timeit
import math
import random

# Timing Tests for Worst Case Mergesort Analysis
def worst_case(n,times):

    workbook= Workbook('Worst-Case-Analysis.xlsx')
    worksheet = workbook.add_worksheet()

    interval= 0.5/times
    t = []
    intervals=[]
    for i in range(times):
        tTemp=0
        for _ in range(10):
            L = create_near_sorted_list(n,i*interval)
            start=timeit.default_timer()
            mergesort_bottom(L)
            end=timeit.default_timer()
            tTemp=tTemp + end-start
        intervals.append(i*interval)
        t.append(tTemp/10)
    for i in range(times):
        worksheet.write(i,0,intervals[i])
        worksheet.write(i,1,t[i])
    workbook.close()

# Timing Tests to Compare Recursive (Top-down) to Iterative (Bottom-up) Mergesort
def top_bottom_tests():
    workbook = Workbook('top-vs-bottom.xlsx')
    worksheet = workbook.add_worksheet()

    top_times =[]
    bottom_times =[]
    for i in range(100, 5100, 100):
        L = create_random_list(i)
        top_time_sum =0
        bot_time_sum =0
        for _ in range(100):
            L1 = L.copy()
            start = timeit.default_timer()
            mergesort(L1)
            end = timeit.default_timer()
            top_time_sum += (end-start)

            L1 = L.copy()
            start = timeit.default_timer()
            mergesort_bottom(L1)
            end = timeit.default_timer()
            bot_time_sum += (end-start)
        top_times.append(top_time_sum/100)
        bottom_times.append(bot_time_sum/100)
    n = 0
    for i in range(50):
        worksheet.write(i, 0, 100+n)
        worksheet.write(i, 1, top_times[i])
        worksheet.write(i, 2, bottom_times[i])
        n += 100
    workbook.close()

# Timing Tests for 2-Way vs. 3-Way Mergesort
def three_way_merge_test():

    workbook = Workbook('three-way-merge.xlsx')
    worksheet = workbook.add_worksheet()

    t1 =[]
    t2 =[]
    for i in range(100, 5100, 100):
        L = create_random_list(i)
        two_way_sum = 0
        three_way_sum = 0
        for _ in range(100):
            L1 = L.copy()
            start = timeit.default_timer()
            mergesort(L1)
            end = timeit.default_timer()
            two_way_sum += (end-start)

            L1 = L.copy()
            start = timeit.default_timer()
            mergesort_three(L1)
            end = timeit.default_timer()
            three_way_sum += (end-start)
        t1.append(two_way_sum/100)
        t2.append(three_way_sum/100)
    n = 0
    for i in range(50):
        worksheet.write(i, 0, 100+n)
        worksheet.write(i, 1, t1[i])
        worksheet.write(i, 2, t2[i])
        n += 100
    workbook.close()

# Timing Tests to Compare 3-Way to Iterative (Bottom-up) Mergesort
def three_way_vs_bottom_test():

    workbook = Workbook('three-vs-bottom.xlsx')
    worksheet = workbook.add_worksheet()

    t1 =[]
    t2 =[]
    for i in range(100, 5100, 100):
        L = create_random_list(i)
        three_way_sum =0
        bot_time_sum =0
        for _ in range(100):
            L1 = L.copy()
            start = timeit.default_timer()
            mergesort_three(L1)
            end = timeit.default_timer()
            three_way_sum += (end-start)

            L1 = L.copy()
            start = timeit.default_timer()
            mergesort_bottom(L1)
            end = timeit.default_timer()
            bot_time_sum += (end-start)
        t1.append(three_way_sum/100)
        t2.append(bot_time_sum/100)
    n = 0
    for i in range(50):
        worksheet.write(i, 0, 100+n)
        worksheet.write(i, 1, t1[i])
        worksheet.write(i, 2, t2[i])
        n += 100
    workbook.close()

# Call Tests

#worst_case(1000,1000)
#top_bottom_tests()
#three_way_merge_test()
#three_way_vs_bottom_test()