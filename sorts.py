from lab4 import create_near_sorted_list, create_random_list

# Iterative (Bottom-Up) Mergesort
def mergesort_bottom(L):
    n = len(L)
    length = 1
    while length < n:
        for lo in range(0, n-length, 2*length):
            mid = lo + length -1
            hi = min(lo+length+length-1, n-1)
            merge_bottom(L, lo, mid, hi)
        length = length*2

# Bottom-Up Helper Function
def merge_bottom(L, start, mid, end):
    temp = []
    for n in range(start, end+1):
        temp.append(L[n])
    
    i = 0
    j = mid - start + 1

    for k in range(start, end+1):
        if i > mid - start:
            L[k] = temp[j]
            j +=1
        elif j > end - start:
            L[k] = temp[i]
            i +=1
        elif temp[j] < temp[i]:
            L[k] = temp[j]
            j+=1
        else:
            L[k] = temp[i]
            i +=1

# Three-Way Mergesort
def mergesort_three(L):
    
    if len(L) <= 1:
        return 
    p1 = len(L)//3
    p2 = (2 * len(L))//3
    left, mid, right = L[:p1], L[p1:p2], L[p2:]

    # Mergesort the three lists
    mergesort_three(left)
    mergesort_three(mid)
    mergesort_three(right)

    # Merge the three sorted lists
    temp = merge_three(left, mid, right)

    #Copy the sorted list to L
    for i in range(len(temp)):
        L[i] = temp[i]

# Three-Way Helper Function
def merge_three(left, mid, right):
    L = []
    i = j = k = 0

    while i < len(left) or j < len(mid) or k < len(right):
        #Check it there's still elements to be merged from left, mid, and/or right
        # Run out of elements from left
        if i >= len(left):
            # Also run out of elements from mid, append right
            if j >= len(mid):
                L.append(right[k])
                k += 1
            # Run out of elements from right, append mid
            elif k >= len(right):
                L.append(mid[j])
                j += 1
            # 2-way merge between mid and right
            else:
                if mid[j] <= right[k]:
                    L.append(mid[j])
                    j += 1
                else:
                    L.append(right[k])
                    k += 1

        # Run out of elements from mid
        elif j >= len(mid):
            # Also run out of elements from right, append left
            if k >= len(right):
                L.append(left[i])
                i += 1
            # 2-way merge between left and right
            else:
                if left[i] <= right[k]:
                    L.append(left[i])
                    i += 1
                else:
                    L.append(right[k])
                    k += 1

        # 2-way merge between left and mid
        elif k >= len(right):
            if left[i] <= mid[j]:
                L.append(left[i])
                i += 1
            else:
                L.append(mid[j])
                j += 1
        
        # Otherwise, general case: 3-way compare
        else:
            l, m, r = left[i], mid[j], right[k]
            # left is smallest
            if l <= m and l <= r:
                L.append(l)
                i += 1
            # mid is smallest
            elif m <= l and m <= r:
                L.append(m)
                j += 1
            # right is smallest
            else: 
                L.append(r)
                k += 1
    return L