import random
def bottom_mergesort(A):
    n = len(A)
    length = 1
    while length < n:
        for lo in range(0, n-length, 2*length):
            mid = lo + length -1
            hi = min(lo+length+length-1, n-1)
            merge_bottom(A, lo, mid, hi)
        length = length*2
    return A

def merge_bottom(A, lo, mid, hi):
    temp = []
    for n in range(lo, hi+1):
        temp.append(A[n])
    
    i = 0
    j = mid - lo + 1

    for k in range(lo, hi+1):
        if i > mid - lo:
            A[k] = temp[j]
            j +=1
        elif j > hi - lo:
            A[k] = temp[i]
            i +=1
        elif temp[j] < temp[i]:
            A[k] = temp[j]
            j+=1
        else:
            A[k] = temp[i]
            i +=1
